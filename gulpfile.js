const gulp = require('gulp');
const ts = require('gulp-typescript');
const tslint = require('gulp-tslint');
const mocha = require('gulp-mocha');
const istanbul = require('gulp-istanbul');

gulp.task('lint', function () {
  return gulp.src([
    '**/*.ts',
    '!node_modules/**/*',
    '!typings/**/*'
  ], {read: true})
             .pipe(tslint({
               formatter: 'msbuild'
             }))
             .pipe(tslint.report({
               emitError: false,
               summarizeFailureOutput: true
             }));
});

gulp.task('pre-test', () => {
  return gulp.src([
    'export/**/*.js',
    '!export/test/**/*'
  ], {read: true})
             .pipe(istanbul())
             .pipe(istanbul.hookRequire());
});

gulp.task('test', [ 'pre-test' ], () => {
  return gulp.src([ 'export/test/**/*.js' ], {read: true})
             .pipe(mocha({}))
             .pipe(istanbul.writeReports({reporters: [ 'lcov', 'text', 'text-summary' ]}))
             .pipe(istanbul.enforceThresholds({thresholds: {global: 90}}))
             .once('error', ()=> process.exit(1))
             .once('end', ()=> process.exit(0))
});