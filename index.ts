import Express = require('express');
import moment = require('moment');
let app = Express();
app.get('/ping', function ( req, res ) {
  res.send({
    time: moment().unix()
  });
});

export = app.listen(8080, function ( err ) {
  if (err) {
    console.error('listen fail %s', err.message);
    process.exit(1);
  } else {
    console.log('listen');
  }
});

