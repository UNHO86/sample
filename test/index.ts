import test = require('supertest');
import server = require('../index');

describe('test', function () {
  it('/ping ', function ( done ) {
    test(server)
      .get('/ping')
      .expect(200)
      .end(() => {
        done();
      });
  });
});