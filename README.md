# sample project

## 구성
 * node.js 6.X.X
   * typescript
   * express
   * mocha
   * gulp
 * MySQL 5.7.X
 * redis 3.2 

## 기능
 * 인증서버
   * 사용자 인증
 * 게임서버
   * 가챠 시스템
 * 운영툴
   * 가챠 테이블 관리
   * 서버 점검
   * 서버 URL 관리
